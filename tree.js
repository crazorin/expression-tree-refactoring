const assert = require("assert");

const PLUS = "+";
const MINUS = "-";
const MULTIPLICATION = "x";
const DIVISION = "÷";

const Node = (value, left, right) => {
  if(typeof value != 'string' && typeof value != 'number') {
    return "The value parameter needs to be either a string or number.";
  }

  if(typeof value == 'string' && ![PLUS, MINUS, MULTIPLICATION, DIVISION].includes(value)) {
   return "If the Node value is a string, it needs to be one of the following character: +, -, x, ÷.";
  }

  if(typeof value == 'string' && (!left || !right)) {
    return "If the Node value is an operator, then neither the left or right parameter can be null.";
  }

  const result = function () {
    switch (this.value) {
      case PLUS:
        return left.result() + right.result();
      case MINUS:
        return left.result() - right.result();
      case MULTIPLICATION:
        return left.result() * right.result();
      case DIVISION:
        return left.result() / right.result();
      default:
        return value;
    }
  };

  const toString = function () {
    switch (this.value) {
      case PLUS:
        return `(${left.toString()} + ${right.toString()})`;
      case MINUS:
        return `(${left.toString()} - ${right.toString()})`;
      case MULTIPLICATION:
        return `(${left.toString()} x ${right.toString()})`;
      case DIVISION:
        return `(${left.toString()} ÷ ${right.toString()})`;
      default:
        return value.toString();
    }
  };

  return {
    value,
    left,
    right,
    result,
    toString
  };
};

// Testing
const tree = Node(
  DIVISION,
  Node(
    PLUS,
    Node(7),
    Node(
      MULTIPLICATION,
      Node(MINUS, Node(3), Node(2)),
      Node(5)
    )
  ),
  Node(6)
);

// Tests
assert.strictEqual(Node(null), "The value parameter needs to be either a string or number.");
assert.strictEqual(Node({ id: 2 }), "The value parameter needs to be either a string or number.");
assert.strictEqual(Node("string"), "If the Node value is a string, it needs to be one of the following character: +, -, x, ÷.");
assert.strictEqual(Node("+", null, "string"), "If the Node value is an operator, then neither the left or right parameter can be null.");
assert.strictEqual(Node("+", "string", null), "If the Node value is an operator, then neither the left or right parameter can be null.");

// Original Tests
assert.strictEqual("((7 + ((3 - 2) x 5)) ÷ 6)", tree.toString());
assert.strictEqual(2, tree.result());
